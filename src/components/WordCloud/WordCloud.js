import { useState } from "react";
import Topic from "./Topic";
import TopicStats from "./TopicStats";
import topics from "../../assets/topics.json";

const WordCloud = () => {
  const [selectedTopic, setSelectedTopic] = useState();

  const selectedTopicCallback = (childData) => {
    setSelectedTopic(childData);
  };

  let topicsArray = [];

  for (let i in topics.topics) {
    topicsArray.push(topics.topics[i]);
  }

  return (
    <div className="wrapper">
      <div className="content">
        <ul className="cloud">
          {topicsArray.map((topic) => (
            <Topic
              key={topic.id}
              topic={topic}
              parentCallBack={selectedTopicCallback}
            />
          ))}
        </ul>
      </div>
      <TopicStats selectedTopic={selectedTopic} />
    </div>
  );
};

export default WordCloud;
