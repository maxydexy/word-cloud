import { Link } from "react-router-dom";

const Topic = ({ topic, parentCallBack }) => {
  const handleOnClick = () => {
    parentCallBack(topic);
  };

  const fontSize = () => {
    let score = topic.volume;
    if (score < 5) return "1";
    if (5 < score && score < 10) return "2";
    if (10 < score && score < 20) return "3";
    if (20 < score && score < 45) return "4";
    if (45 < score && score < 100) return "5";
    if (100 < score) return "6";
  };

  const fontColor = () => {
    let score = topic.sentimentScore;
    if (score < 40) return "red";
    if (score > 60) return "green";
    else {
      return "grey";
    }
  };

  return (
    <li>
      <Link
        className="link-topic"
        to="#"
        data-weight={fontSize()}
        style={{
          color: fontColor(),
        }}
        onClick={handleOnClick}
      >
        {topic.label}
      </Link>
    </li>
  );
};

export default Topic;
