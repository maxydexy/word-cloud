const TopicStats = ({ selectedTopic }) => {
  return (
    <div className="stats">
      {selectedTopic ? (
        <div>
          <h4>Information on topic: {selectedTopic.label}</h4>
          <p className="total">Total Mentions: {selectedTopic.volume}</p>
          <p className="mentions">
            Positive Mentions:
            <span
              style={{
                color: "green",
              }}
            >
              {!selectedTopic.sentiment.positive
                ? 0
                : selectedTopic.sentiment.positive}
            </span>
          </p>
          <p className="mentions">
            Neutral Mentions:
            {!selectedTopic.sentiment.neutral
              ? 0
              : selectedTopic.sentiment.neutral}
          </p>
          <p className="mentions">
            Negative Mentions:
            <span
              style={{
                color: "red",
              }}
            >
              {!selectedTopic.sentiment.negative
                ? 0
                : selectedTopic.sentiment.negative}
            </span>
          </p>
        </div>
      ) : (
        <p></p>
      )}
    </div>
  );
};

export default TopicStats;
