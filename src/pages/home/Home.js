import WordCloud from "../../components/WordCloud/WordCloud";

const Home = () => {
  return (
    <div>
      <h1 className="title">My Topics Challenge</h1>
      <WordCloud />
    </div>
  );
};

export default Home;
