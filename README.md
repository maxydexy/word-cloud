# Word Cloud

Word cloud is a challenge project.

![word-cloud-img](/uploads/725ff1592056c7a136a6ebdab881da8e/word-cloud-img.jpg)

Application reads data from topics.json and then displays them in the word cloud.
Clicking on the topic displays information about sentiment score and popularity on the right.


## Local development    


To build the app locally open word-cloud and install node modules

```sh
npm install
# or
yarn install
```

After installation is finished run applicaiton.

```sh
npm start
# or
yarn run
```


